package com.balad.test.aroundme.di.module

import com.balad.test.aroundme.ui.adapter.VenueAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped

@InstallIn(FragmentComponent::class)
@Module
object FragmentModule {

    @Provides
    @FragmentScoped
    fun provideVenueAdapter(): VenueAdapter {
        return VenueAdapter()
    }
}