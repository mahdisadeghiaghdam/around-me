package com.balad.test.aroundme.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_remote_key")
data class RemoteKey(@PrimaryKey val repoId: Int, val prevKey: Int?, val nextKey: Int?) {
}