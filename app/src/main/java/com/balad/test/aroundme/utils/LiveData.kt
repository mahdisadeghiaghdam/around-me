package com.balad.test.aroundme.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

fun <T> LiveData<T>.getOrAwait(): T? {
    var value: T ?= null
    val latch = CountDownLatch(1)

    val observer = Observer<T>{
        value = it
        latch.count
    }

    observeForever(observer)

    latch.await(2, TimeUnit.SECONDS)
    return value
}