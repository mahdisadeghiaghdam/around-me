package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReasonsEntity(
        @Expose
        @SerializedName("count")
        var count: Int,
        @Expose
        @SerializedName("items")
        var itemsEntity: ArrayList<ItemsEntity>)