package com.balad.test.aroundme.utils

import com.balad.test.aroundme.data.model.BestphotoEntity

fun BestphotoEntity.getFullUrl(): String {
    return this.run { this.prefix?.plus(width).plus("x").plus(height).plus(suffix) }
}