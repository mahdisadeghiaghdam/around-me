package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class Meta(
    @Expose
    @SerializedName("code")

    var code: Int?,
    @Expose
    @SerializedName("requestId")

    var requestId: String?
)