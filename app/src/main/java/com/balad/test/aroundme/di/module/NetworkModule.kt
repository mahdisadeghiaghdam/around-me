package com.balad.test.aroundme.di.module

import android.content.SharedPreferences
import com.balad.test.aroundme.data.network.VenuesApiService
import com.balad.test.aroundme.data.network.policy.ApiPolicy
import com.balad.test.aroundme.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Provides
    @Singleton
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(Constants.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Provides
    @Singleton
    fun provideVenueApiService(retrofitBuilder: Retrofit.Builder): VenuesApiService {
        return retrofitBuilder.build()
            .create(VenuesApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideApiPolicy(sharedPreferences: SharedPreferences, gson: Gson): ApiPolicy {
        return ApiPolicy(sharedPreferences)
    }

}