package com.balad.test.aroundme.data.remote

import android.location.Location
import android.util.Log
import androidx.paging.*
import androidx.room.withTransaction
import com.balad.test.aroundme.data.local.MainDataBase
import com.balad.test.aroundme.data.model.RemoteKey
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.network.VenuesApiService
import com.balad.test.aroundme.data.network.policy.ApiPolicy
import com.balad.test.aroundme.utils.toComaFormat
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class VenueRemoteMediator private constructor(
    private val venuesApiService: VenuesApiService,
    private val mainDataBase: MainDataBase,
    private val date: String,
    private val intent: Venue.Intent,
    private val radius: Int = 100,
    private val limit: Int = 200,
    private val offset: Int = 0,
    private val apiPolicy: ApiPolicy,
    private val location: com.balad.test.aroundme.data.model.Location
) : RemoteMediator<Int, Venue>() {
    override suspend fun load(loadType: LoadType, state: PagingState<Int, Venue>): MediatorResult {
        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> getRemoteKeys()
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    state.lastItemOrNull()
                        ?: return MediatorResult.Success(endOfPaginationReached = true)
                    getRemoteKeys()
                }
            }

            val response = if (apiPolicy.accessCallRequest(location))venuesApiService.getSearchVenuesBrowse(
                date,
                location.toComaFormat(),
                intent.name,
                radius,
                state.config.pageSize,
                offset
            ) else null

            val listing = response?.body()?.response?.venues

            val isEmpty = listing?.size ?: 0 == 0

            if (!isEmpty) {
                mainDataBase.withTransaction {
                    mainDataBase.remoteDao().insert(RemoteKey(0, loadKey?.prevKey, loadKey?.nextKey))
                    mainDataBase.venueDao().clearVenues()
                    mainDataBase.venueDao().insert(listing?.toList()!!)
                }
            }

            MediatorResult.Success(endOfPaginationReached = loadKey?.nextKey == null)
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }
    }

    /**
     * Get remote key for paginate local database
     */
    private suspend fun getRemoteKeys(): RemoteKey? {
        return mainDataBase.remoteDao().getRemoteKeysById(0).firstOrNull()
    }

    /**
     * build VenueRemoteMediator
     */
    class Builder(
        private val venuesApiService: VenuesApiService,
        private val mainDataBase: MainDataBase,
        private val date: String,
        private val apiPolicy: ApiPolicy,
        private val location: com.balad.test.aroundme.data.model.Location
    ) : IVenueRemoteBuilder, IVenueRemoteMatch, IVenueRemoteCheckIn, IVenueRemoteBrowse {
        private var intent: Venue.Intent = Venue.Intent.browse
        private var radius: Int = 100
        private var limit: Int = 200
        private var offset: Int = 0

        override fun selectBrowse(): IVenueRemoteBrowse = this

        override fun selectCheckIn(): IVenueRemoteCheckIn = this

        override fun selectGlobal(): IVenueRemoteGlobal  = this

        override fun selectMatch(): IVenueRemoteMatch  = this

        override fun setRadius(radius: Int): IVenueRemoteBrowse = apply {
            this.radius = radius
        }

        override fun setLimit(limit: Int): IVenueRemoteGlobal = apply {
            this.limit = limit
        }

        override fun build(): VenueRemoteMediator =
            VenueRemoteMediator(
                venuesApiService,
                mainDataBase,
                date,
                intent,
                radius,
                limit,
                offset,
                apiPolicy,
                location
            )
    }
}