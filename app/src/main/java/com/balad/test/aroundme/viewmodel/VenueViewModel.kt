package com.balad.test.aroundme.viewmodel

import android.app.Application
import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.PagingSource
import androidx.paging.cachedIn

import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.repo.VenueRepo
import com.balad.test.aroundme.utils.Constants
import com.balad.test.aroundme.utils.toComaFormat
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class VenueViewModel @Inject constructor(private val venueRepo: VenueRepo, private val gson: Gson) : ViewModel() {
    fun getVenueById(id: String): LiveData<Venue>? {
        return venueRepo.getVenue(id)
    }

    fun getVenueList(location: com.balad.test.aroundme.data.model.Location, limit: Int = 200, intent: Venue.Intent = Venue.Intent.browse, radius: Int = 100): Flow<PagingData<Venue>>{
        return venueRepo.getVenueList(
            limit,
            intent,
            radius,
            location
        ).cachedIn(viewModelScope)
    }

    fun getLastLocation(): com.balad.test.aroundme.data.model.Location? {
        return venueRepo.getLastLocation()
    }

    fun setLastLocation(location: Location) {
        venueRepo.setLastLocation(com.balad.test.aroundme.data.model.Location(lat = location.latitude, lng = location.longitude))
    }

    fun removeLastLocation() {
        venueRepo.removeLastLocation()
    }
}