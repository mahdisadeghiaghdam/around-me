package com.balad.test.aroundme.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import com.balad.test.aroundme.R
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.utils.getFullAddress
import kotlinx.android.synthetic.main.adapter_venue_item.view.*

class VenueAdapter: PagingDataAdapter<Venue, VenueAdapter.ViewHolder>(VenueDiffUtil()) {
    var onItemClickListener: ((Venue) -> Unit) ?= null
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(venue: Venue, onItemClickListener: ((Venue) -> Unit) ?= null) {
            itemView.text_name?.text = venue.name
            itemView.text_content?.text = venue.getFullAddress()

            itemView.setOnClickListener {
                onItemClickListener?.invoke(venue)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, onItemClickListener) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_venue_item, parent, false))
    }

    fun withMySpecificFooter(
        footer: LoadStateAdapter<*>
    ): ConcatAdapter {
        addLoadStateListener { loadStates ->
            footer.loadState = when (loadStates.refresh) {
                is LoadState.NotLoading -> loadStates.append
                else -> loadStates.refresh
            }
        }
        return ConcatAdapter(this, footer)
    }
}