package com.balad.test.aroundme.data.repo

import android.os.Message

sealed class Status {
    class Loading : Status()
    class Error(code: Int?, message: String?) : Status()
    class Success<T>(date: T?) : Status()
}
