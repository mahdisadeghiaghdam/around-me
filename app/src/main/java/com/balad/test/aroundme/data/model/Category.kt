package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


class Category(
    @Expose
    @SerializedName("icon")

    var icon: Icon?,
    @Expose
    @SerializedName("id")

    var id: String?,
    @Expose
    @SerializedName("name")

    var name: String?,
    @Expose
    @SerializedName("pluralName")

    var pluralName: String?,
    @Expose
    @SerializedName("primary")

    var primary: Boolean?,
    @Expose
    @SerializedName("shortName")

    var shortName: String?
)