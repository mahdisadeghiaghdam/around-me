package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.Location
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class LocationConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: Location?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): Location ?{
            val listType = object : TypeToken<Location?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}