package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class Location(
    @Expose
    @SerializedName("address")

    var address: String ?= null,
    @Expose
    @SerializedName("cc")

    var cc: String ?= null,
    @Expose
    @SerializedName("city")

    var city: String ?= null,
    @Expose
    @SerializedName("country")

    var country: String ?= null,
    @Expose
    @SerializedName("distance")

    var distance: Int ?= null,
    @Expose
    @SerializedName("formattedAddress")

    var formattedAddress: ArrayList<String> ?= null,
    @Expose
    @SerializedName("labeledLatLngs")

    var labeledLatLngs: ArrayList<LabeledLatLng> ?= null,
    @Expose
    @SerializedName("lat")

    var lat: Double ?= null,
    @Expose
    @SerializedName("lng")

    var lng: Double ?= null,
    @Expose
    @SerializedName("state")

    var state: String ?= null
)