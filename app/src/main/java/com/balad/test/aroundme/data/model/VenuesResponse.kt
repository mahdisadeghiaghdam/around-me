package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class VenuesResponse(
    @Expose
    @SerializedName("meta")

    var meta: Meta?,
    @Expose
    @SerializedName("response")

    var response: Response?
)