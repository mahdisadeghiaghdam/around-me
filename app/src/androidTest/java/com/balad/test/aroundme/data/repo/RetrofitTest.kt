package com.balad.test.aroundme.data.repo

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.network.VenuesApiService
import com.balad.test.aroundme.utils.Constants
import com.google.gson.GsonBuilder
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RetrofitTest: TestCase() {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var retrofit: Retrofit.Builder
    lateinit var apiEndpoints: VenuesApiService
    @Before
    public override fun setUp() {
        retrofit = Retrofit.Builder()
            .baseUrl(Constants.API_BASE_URL)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .create()
                )
            )

        apiEndpoints = retrofit.build().create(VenuesApiService::class.java)
    }
    @Test
    fun get_Venue_list_test() {
        try {
            runBlocking {
                val response = apiEndpoints.getSearchVenuesBrowse(
                    "20210802",
                    "35.780191,51.3720896",
                    "browse",
                    100,
                    100,
                    0
                )
                val body = response?.body()

                assertTrue(response?.code() == 200)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            assertTrue(false)
        }
    }

    @Test
    fun get_Venue_detail() {
        try {
            runBlocking {
                val response = apiEndpoints.getDetailOfVenues(
                    "4f3c2736e4b01e3346b38bdd",
                    "20210802"
                )
                response.enqueue(object : Callback<Venue>{
                    override fun onResponse(call: Call<Venue>, response: Response<Venue>) {
                        assertTrue(response.isSuccessful)
                    }

                    override fun onFailure(call: Call<Venue>, t: Throwable) {
                        assertTrue(false)
                    }

                })
            }
        } catch (e: IOException) {
            e.printStackTrace()
            assertTrue(false)
        }
    }
}