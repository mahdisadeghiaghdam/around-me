package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

 class LikesEntity(
        @Expose
        @SerializedName("count")
        var count: Int,
        @Expose
        @SerializedName("groups")
        var groupsEntity: ArrayList<GroupsEntity>,
        @Expose
        @SerializedName("summary")
        val summary: String)