package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HerenowEntity(
        @Expose
        @SerializedName("count")
        var count: Int,
        @Expose
        @SerializedName("summary")
        val summary: String,
        @Expose
        @SerializedName("groups")
        var groups: ArrayList<String>)