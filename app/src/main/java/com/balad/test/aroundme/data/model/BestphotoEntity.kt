package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BestphotoEntity(
        @Expose
        @SerializedName("id")
        val id: String?,
        @Expose
        @SerializedName("createdAt")
        var createdat: Int?,
        @Expose
        @SerializedName("source")
        var source: SourceEntity?,
        @Expose
        @SerializedName("prefix")
        val prefix: String?,
        @Expose
        @SerializedName("suffix")
        val suffix: String?,
        @Expose
        @SerializedName("width")
        var width: Int?,
        @Expose
        @SerializedName("height")
        var height: Int?,
        @Expose
        @SerializedName("visibility")
        val visibility: String?
)