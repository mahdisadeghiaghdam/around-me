package com.balad.test.aroundme.data.repo

import android.content.SharedPreferences
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.balad.test.aroundme.data.local.MainDataBase
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.model.VenueDetailResponse
import com.balad.test.aroundme.data.network.VenuesApiService
import com.balad.test.aroundme.data.network.policy.ApiPolicy
import com.balad.test.aroundme.data.remote.RemoteMediatorFactory
import com.balad.test.aroundme.data.remote.VenueRemoteMediator
import com.balad.test.aroundme.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import kotlin.math.ln

class VenueRepo @Inject constructor(
    private val mainDataBase: MainDataBase,
    private val venuesApiService: VenuesApiService,
    private val date: String,
    private val sharedPreferences: SharedPreferences,
    private val apiPolicy: ApiPolicy
) {
    /**
     * Get detail of venue by id
     * @param id Selected venue id
     * @param status A livedata for show of state of request
     *
     * @return Selected venue id from local database
     */
    fun getVenue(id: String, status: MutableLiveData<Status>? = null): LiveData<Venue>? {
        status?.postValue(Status.Loading())
        venuesApiService.getDetailOfVenues(venuesId = id, date).enqueue(object : Callback<VenueDetailResponse> {
            override fun onResponse(call: Call<VenueDetailResponse>, response: Response<VenueDetailResponse>) {
                if (response.code() == 200) {
                    response.body()?.response?.venue?.let { venue ->
                        runBlocking { mainDataBase.venueDao().insert(venue) }
                        status?.postValue(Status.Success(venue))
                    }
                } else {
                    status?.postValue(Status.Error(response.code(), response.message()))
                }
            }

            override fun onFailure(call: Call<VenueDetailResponse>, t: Throwable) {
                status?.postValue(Status.Error(10, t.message))
            }
        })
        return mainDataBase.venueDao().getVenueById(id)
    }

    /**
     * Set RemoteMediator to paging3 for offline scenario
     * @param ll Latitude and longitude of the user’s location. with Latitude, longitude format
     * @param intent Find venues within a given area
     * @param radius limit results to venues within this many meters of the specified location.
     * @param limit Number of results to return, up to 50.
     *
     * @return Selected venue id from local database
     */
    @OptIn(ExperimentalPagingApi::class)
    fun getVenueList(
        limit: Int = 100,
        intent: Venue.Intent,
        radius: Int,
        location: com.balad.test.aroundme.data.model.Location
    ): Flow<PagingData<Venue>> {
        return Pager(
            PagingConfig(
                pageSize = limit,
                enablePlaceholders = false
            ),
            remoteMediator = RemoteMediatorFactory.createRemoteMediator(
                venuesApiService,
                mainDataBase,
                date,
                intent,
                radius,
                limit,
                0,
                apiPolicy,
                location
            ),
            pagingSourceFactory = { mainDataBase.venueDao().getDataList() }
        ).flow
    }

    fun getLastLocation(): com.balad.test.aroundme.data.model.Location {
        val lat = sharedPreferences.getString(Constants.PREF_LAST_LOCATION_LAT, null)
        val lng = sharedPreferences.getString(Constants.PREF_LAST_LOCATION_LNG, null)
        return com.balad.test.aroundme.data.model.Location(lat = lat?.toDouble(), lng = lng?.toDouble())
    }

    fun setLastLocation(location: com.balad.test.aroundme.data.model.Location) {
        sharedPreferences.edit().putString(Constants.PREF_LAST_LOCATION_LAT, location.lat.toString()).apply()
        sharedPreferences.edit().putString(Constants.PREF_LAST_LOCATION_LNG, location.lng.toString()).apply()
    }

    fun removeLastLocation() {
        sharedPreferences.edit().remove(Constants.PREF_LAST_LOCATION_LAT).apply()
        sharedPreferences.edit().remove(Constants.PREF_LAST_LOCATION_LNG).apply()
    }
}