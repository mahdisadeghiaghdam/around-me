package com.balad.test.aroundme.data.model


import androidx.room.Entity
import androidx.room.PrimaryKey

import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

//@Parcelize
@Entity(tableName = "tbl_venue")
class Venue(
        @Expose
        @SerializedName("categories")

        var categories: ArrayList<Category> ?= null,
        @Expose
        @SerializedName("hasPerk")

        var hasPerk: Boolean ?= null,
        @Expose
        @SerializedName("id")
        @PrimaryKey

        var id: String,
        @Expose
        @SerializedName("location")

        var location: Location ?= null,
        @Expose
        @SerializedName("name")

        var name: String ?= null,
        @Expose
        @SerializedName("referralId")

        var referralId: String ?= null,


        @Expose
        @SerializedName("canonicalUrl")
        var canonicalUrl: String ?= null,


        @Expose
        @SerializedName("stats")
        var stats: StatsEntity ?= null,
        @Expose
        @SerializedName("likes")
        var likes: LikesEntity ?= null,
        @Expose
        @SerializedName("dislike")
        var dislike: Boolean ?= null,
        @Expose
        @SerializedName("ok")
        var ok: Boolean ?= null,
        @Expose
        @SerializedName("specials")
        var specials: SpecialsEntity ?= null,
        @Expose
        @SerializedName("reasons")
        var reasons: ReasonsEntity ?= null,
        @Expose
        @SerializedName("hereNow")
        var hereNow: HerenowEntity ?= null,
        @Expose
        @SerializedName("bestPhoto")
        var bestPhoto: BestphotoEntity ?= null){

        enum class Intent{
                browse,
                checkin,
                global,
                match

        }
}