package com.balad.test.aroundme.data.local

import androidx.lifecycle.LiveData
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: List<T>)

    @Delete
    suspend fun delete(model: T): Int

    @Update
    suspend fun update(model: T): Int
}