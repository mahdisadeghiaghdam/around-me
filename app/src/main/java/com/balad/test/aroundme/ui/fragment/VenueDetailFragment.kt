package com.balad.test.aroundme.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.balad.test.aroundme.R
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.utils.getFullAddress
import com.balad.test.aroundme.utils.getFullUrl
import com.balad.test.aroundme.viewmodel.VenueViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_venue_detail.*
import kotlinx.android.synthetic.main.fragment_venue_detail.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

@AndroidEntryPoint
class VenueDetailFragment: Fragment(R.layout.fragment_venue_detail) {

    private var venue: Venue ?= null
    private val venueViewModel: VenueViewModel by viewModels()

    private val args: VenueDetailFragmentArgs by navArgs()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe()

        text_website?.setOnClickListener {
            venue?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(venue?.canonicalUrl))
                startActivity(browserIntent)
            }
        }
    }


    /**
     * Observe venue and update ui
     */
    private fun observe() {
        CoroutineScope(Main).launch {
            venueViewModel.getVenueById(args.venueId)?.observe(viewLifecycleOwner, Observer {
                venue = it
                text_name?.text = it.name
                text_category?.text = it.categories?.firstOrNull()?.name ?: ""
                text_address?.text = it.getFullAddress()
                text_like?.text = it.likes?.summary ?: "No like"
                text_website?.text = it?.run {
                    text_website?.isVisible = true
                    this.canonicalUrl
                } ?: kotlin.run {
                    text_website?.isVisible = false
                    ""
                }

                Glide.with(this@VenueDetailFragment)
                    .load(it.bestPhoto?.getFullUrl())
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade(300))
                    .into(img_best_photo)
            })
        }
    }
}