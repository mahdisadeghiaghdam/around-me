package com.balad.test.aroundme.data.network

import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.model.VenueDetailResponse
import com.balad.test.aroundme.data.model.VenuesResponse
import com.balad.test.aroundme.utils.Constants
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface VenuesApiService {
    /**
     * Find venues within a given area
     * @param date Date with YYYYMMdd format
     * @param ll Latitude and longitude of the user’s location. with Latitude, longitude format
     * @param intent Find venues within a given area
     * @param radius limit results to venues within this many meters of the specified location.
     * @param limit Number of results to return, up to 50.
     * @return list of venue
     */
    @GET("v2/venues/search?client_id=${Constants.CLIENT_ID}&client_secret=${Constants.CLIENT_SECRET}")
    suspend fun getSearchVenuesBrowse(
        @Query("v") date: String,
        @Query("ll") ll: String,
        @Query("intent") intent: String,
        @Query("radius") radius: Int,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int = 0
    ): Response<VenuesResponse>?

    /**
     * Get detail of venue
     * @param date Date with YYYYMMdd format
     * @param venuesId Venue id that you want to get detail
     * @return detail of venue
     */
    @GET("/v2/venues/{venues_id}?client_id=${Constants.CLIENT_ID}&client_secret=${Constants.CLIENT_SECRET}")
    fun getDetailOfVenues(
        @Path("venues_id") venuesId: String,
        @Query("v") date: String
    ): Call<VenueDetailResponse>
}