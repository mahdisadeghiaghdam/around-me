package com.balad.test.aroundme.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.balad.test.aroundme.R
import kotlinx.android.synthetic.main.adapter_loading.view.*

class VenueLoadingState: LoadStateAdapter<VenueLoadingState.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(loadState: LoadState) {
            itemView.pgb_loading?.visibility = View.VISIBLE
        }
    }
    override fun onBindViewHolder(holder: VenueLoadingState.ViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): VenueLoadingState.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_loading, parent, false))
    }

}