package com.balad.test.aroundme.data.repo

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.balad.test.aroundme.data.local.MainDataBase
import com.balad.test.aroundme.data.local.dao.VenueDao
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.network.VenuesApiService
import com.balad.test.aroundme.utils.Constants
import com.balad.test.aroundme.utils.getOrAwait
import com.google.gson.GsonBuilder
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(AndroidJUnit4::class)
class VenueRepoClassTest : TestCase() {

    private lateinit var db: MainDataBase
    private lateinit var dao: VenueDao
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, MainDataBase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.venueDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun insert_venue_test() {
        runBlocking {
            val venue = Venue(id = "test")
            val insertResult = dao.insert(venue)

            assertTrue(insertResult != -1L)
        }
    }


    @Test
    fun get_Venue_test() {
        runBlocking {
            val venue = Venue(id = "test")
            dao.insert(venue)
        }
        val venue = dao.getVenueById("test").getOrAwait()
        assertTrue(venue?.id == "test")
    }

}