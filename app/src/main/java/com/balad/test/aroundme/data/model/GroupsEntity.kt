package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GroupsEntity(
        @Expose
        @SerializedName("type")
        val type: String,
        @Expose
        @SerializedName("count")
        var count: Int)