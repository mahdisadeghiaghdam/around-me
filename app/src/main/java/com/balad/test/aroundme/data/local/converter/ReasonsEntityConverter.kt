package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.ReasonsEntity
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class ReasonsEntityConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: ReasonsEntity?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): ReasonsEntity ?{
            val listType = object : TypeToken<ReasonsEntity?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}