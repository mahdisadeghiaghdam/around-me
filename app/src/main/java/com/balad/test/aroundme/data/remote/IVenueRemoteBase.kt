package com.balad.test.aroundme.data.remote

interface IVenueRemoteBase {
    fun selectBrowse(): IVenueRemoteBrowse
    fun selectCheckIn(): IVenueRemoteCheckIn
    fun selectGlobal(): IVenueRemoteGlobal
    fun selectMatch(): IVenueRemoteMatch
    fun build(): VenueRemoteMediator
}