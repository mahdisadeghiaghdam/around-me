package com.balad.test.aroundme.ui.adapter

import androidx.paging.DifferCallback
import androidx.recyclerview.widget.DiffUtil
import com.balad.test.aroundme.data.model.Venue

class VenueDiffUtil: DiffUtil.ItemCallback<Venue>() {
    override fun areItemsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem.location?.lat == newItem.location?.lat && oldItem.location?.lng == newItem.location?.lng
    }
}