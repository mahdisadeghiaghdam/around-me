package com.balad.test.aroundme.di.module

import android.content.Context
import com.balad.test.aroundme.service.LocationService
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ServiceScoped
import javax.inject.Singleton

@InstallIn(ServiceComponent::class)
@Module
object ServiceModule {

    @Provides
    @ServiceScoped
    fun provideLocationRequest(): LocationRequest{
        return LocationRequest.create().apply {
            interval = LocationService.DEFAULT_INTERVAL_TIME * 30
            fastestInterval = LocationService.DEFAULT_INTERVAL_TIME * 5
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }
    }

    @Provides
    @ServiceScoped
    fun provideFusedLocationProviderClient(@ApplicationContext context: Context): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }
}