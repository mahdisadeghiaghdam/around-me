package com.balad.test.aroundme.data.remote

interface IVenueRemoteGlobal: IBuilder {
    fun setLimit(limit: Int): IVenueRemoteGlobal
}