package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ItemsEntity(
        @Expose
        @SerializedName("summary")
        val summary: String,
        @Expose
        @SerializedName("type")
        val type: String,
        @Expose
        @SerializedName("reasonName")
        val reasonname: String)