package com.balad.test.aroundme.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.balad.test.aroundme.R
import com.balad.test.aroundme.service.BroadCastHandler
import com.balad.test.aroundme.service.LocationService
import dagger.hilt.android.AndroidEntryPoint
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onDestroy() {
        LocationService.stopService(this.applicationContext)
        super.onDestroy()
    }

}