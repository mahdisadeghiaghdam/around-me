package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StatsEntity(
        @Expose
        @SerializedName("tipCount")
        var tipcount: Int)