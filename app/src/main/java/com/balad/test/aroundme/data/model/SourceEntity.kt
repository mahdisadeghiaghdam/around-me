package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SourceEntity(
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("url")
        val url: String)