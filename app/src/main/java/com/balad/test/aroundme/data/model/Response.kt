package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

 class Response(
    @Expose
    @SerializedName("venues")

    var venues: ArrayList<Venue>?
)