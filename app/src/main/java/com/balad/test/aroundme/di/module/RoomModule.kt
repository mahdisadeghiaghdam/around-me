package com.balad.test.aroundme.di.module

import android.content.Context
import androidx.room.Room
import com.balad.test.aroundme.data.local.MainDataBase
import com.balad.test.aroundme.data.local.dao.VenueDao
import com.balad.test.aroundme.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {
    @Provides
    @Singleton
    fun provideMainDb(@ApplicationContext context: Context): MainDataBase {
        return Room.databaseBuilder(
            context,
            MainDataBase::class.java,
            Constants.MAIN_DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideVenueDao(mainDataBase: MainDataBase): VenueDao{
        return mainDataBase.venueDao()
    }
}