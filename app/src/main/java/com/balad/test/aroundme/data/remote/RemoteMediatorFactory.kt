package com.balad.test.aroundme.data.remote

import com.balad.test.aroundme.data.local.MainDataBase
import com.balad.test.aroundme.data.model.Location
import com.balad.test.aroundme.data.model.Venue
import com.balad.test.aroundme.data.network.VenuesApiService
import com.balad.test.aroundme.data.network.policy.ApiPolicy

class RemoteMediatorFactory {

    companion object {
        /**
         * Create remote mediator according to intent
         */
        fun createRemoteMediator(
            venuesApiService: VenuesApiService,
            mainDataBase: MainDataBase,
            date: String,
            intent: Venue.Intent = Venue.Intent.browse,
            radius: Int = 100,
            limit: Int = 200,
            offset: Int = 0,
            apiPolicy: ApiPolicy,
            location: Location
        ): VenueRemoteMediator {
            return with(
                VenueRemoteMediator.Builder(
                    venuesApiService,
                    mainDataBase,
                    date,
                    apiPolicy,
                    location
                )
            ) {
                when (intent) {
                    Venue.Intent.browse -> createBrowse(this, limit, radius)
                    Venue.Intent.checkin -> createCheckIn(this, limit, radius)
                    Venue.Intent.match -> createMatch(this)
                    Venue.Intent.global -> createGlobal(this, limit)
                }
            }
        }

        /**
         * Create Browse Venue Remote Mediator
         *
         * Find venues within a given area. Browse searches an entire region instead of only finding
         * venues closest to a point. A region to search can be defined by including either the ll
         * and radius parameters, or the sw and ne. The region will be circular if you include the ll
         * and radius parameters, or a bounding box if you include the sw and ne parameters.
         *
         * @param radius limit results to venues within this many meters of the specified location.
         * @param limit Number of results to return, up to 50.
         *
         * @return VenueRemoteMediator
         */
        private fun createBrowse(
            builder: VenueRemoteMediator.Builder,
            limit: Int,
            radius: Int
        ): VenueRemoteMediator {
            return builder.selectBrowse()
                .setRadius(radius)
                .setLimit(limit)
                .build()
        }

        /**
         * Create CheckIn Venue Remote Mediator
         *
         * Finds venues that the current user (or, for userless requests, a typical user) is likely
         * to checkin to at the provided ll, at the current moment in time.
         *
         * @param radius limit results to venues within this many meters of the specified location.
         * @param limit Number of results to return, up to 50.
         *
         * @return VenueRemoteMediator
         */
        private fun createCheckIn(
            builder: VenueRemoteMediator.Builder,
            limit: Int,
            radius: Int
        ): VenueRemoteMediator {
            return builder.selectCheckIn()
                .setRadius(radius)
                .setLimit(limit)
                .build()
        }

        /**
         * Create Global Venue Remote Mediator
         * Finds the most globally relevant venues for the search, independent of location.
         * Ignores all parameters other than query and limit.
         *
         * @param limit Number of results to return, up to 50.
         *
         * @return VenueRemoteMediator
         */
        private fun createGlobal(
            builder: VenueRemoteMediator.Builder,
            limit: Int
        ): VenueRemoteMediator {
            return builder.selectGlobal()
                .setLimit(limit)
                .build()
        }

        /**
         * Create Match Venue Remote Mediator
         *
         * Finds a venue that is the near-exact match for the given parameters. This intent is primarily
         * used when trying to harmonize an existing place database with Foursquare’s and is highly
         * sensitive to the provided location. The result will take into account distance and spelling
         * variations. name and ll are the only required parameters for this intent, but we also suggest
         * sending phone, address, city, state, zip, and twitter for better results. There’s no specified
         * format for these parameters—we do our best to normalize them and drop them from the search
         * if unsuccessful.
         *
         * @return VenueRemoteMediator
         */
        private fun createMatch(builder: VenueRemoteMediator.Builder): VenueRemoteMediator {
            return builder.selectMatch().build()
        }
    }
}