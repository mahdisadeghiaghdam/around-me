package com.balad.test.aroundme.utils

import com.balad.test.aroundme.data.model.Venue

fun Venue.getFullAddress(): String {
    return this.location?.run { "${this.country}, ${this.state}, ${this.city}, ${this.address}" }
        ?.replace(", null", "") ?: ""
}