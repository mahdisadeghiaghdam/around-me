package com.balad.test.aroundme.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.balad.test.aroundme.data.local.BaseDao
import com.balad.test.aroundme.data.model.RemoteKey

@Dao
interface RemoteDao: BaseDao<RemoteKey> {
    @Query("DELETE FROM tbl_remote_key")
    suspend fun clearAllKeys()

    @Query("SELECT * FROM tbl_remote_key WHERE repoId=:repoId")
    suspend fun getRemoteKeysById(repoId: Int): List<RemoteKey>
}