package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.BestphotoEntity
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class BestphotoEntityConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: BestphotoEntity?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): BestphotoEntity ?{
            val listType = object : TypeToken<BestphotoEntity?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}