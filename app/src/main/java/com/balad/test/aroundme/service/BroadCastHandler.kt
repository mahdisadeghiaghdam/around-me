package com.balad.test.aroundme.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location

class BroadCastHandler {
    companion object{
        private const val BROADCAST_UPDATE_LOCATION = "BROADCAST_UPDATE_LOCATION"
        private const val BUNDLE_LOCATION = "BUNDLE_LOCATION"

        /**
         * send updated location broadcast to context which registered updated location broadcast
         * @param context
         * @param location updated location
         *
         */
        fun sendUpdatedLocation(context: Context, location: Location){
            context.sendBroadcast(Intent(BROADCAST_UPDATE_LOCATION).apply {
                putExtra(BUNDLE_LOCATION, location)
            })
        }


        /**
         * register updated location broadcast to context
         * @param context
         * @param broadCastReceiver
         *
         */
        fun registerUpdateLocation(context: Context, broadCastReceiver: BroadcastReceiver){
            context.registerReceiver(broadCastReceiver, IntentFilter(BROADCAST_UPDATE_LOCATION))
        }


        /**
         * un register updated location broadcast to context
         * @param context
         * @param broadCastReceiver
         *
         */
        fun unRegisterUpdateLocation(context: Context, broadCastReceiver: BroadcastReceiver){
            context.unregisterReceiver(broadCastReceiver)
        }


        /**
         * get location bundle of broadcast receiver intent
         * @param intent intent that sent from broadcast receiver
         *
         */
        fun getLocation(intent: Intent?): Location? {
            return intent?.getParcelableExtra<Location>(BUNDLE_LOCATION)
        }
    }
}