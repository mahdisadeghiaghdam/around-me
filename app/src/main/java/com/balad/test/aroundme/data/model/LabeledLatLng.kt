package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class LabeledLatLng(
    @Expose
    @SerializedName("label")

    var label: String?,
    @Expose
    @SerializedName("lat")

    var lat: Double?,
    @Expose
    @SerializedName("lng")

    var lng: Double?
)