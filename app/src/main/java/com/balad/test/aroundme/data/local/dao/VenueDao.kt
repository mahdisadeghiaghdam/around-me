package com.balad.test.aroundme.data.local.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import com.balad.test.aroundme.data.local.BaseDao
import com.balad.test.aroundme.data.model.Venue

@Dao
interface VenueDao : BaseDao<Venue>{

    @Query("SELECT * FROM tbl_venue WHERE id=:id")
    fun getVenueById(id: String): LiveData<Venue>

    @Query("SELECT * FROM tbl_venue ")
    fun getDataList(): PagingSource<Int, Venue>

    @Query("DELETE FROM tbl_venue ")
    fun clearVenues()
}