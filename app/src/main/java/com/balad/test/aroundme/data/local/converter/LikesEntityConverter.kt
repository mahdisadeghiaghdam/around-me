package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.LikesEntity
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class LikesEntityConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: LikesEntity?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): LikesEntity ?{
            val listType = object : TypeToken<LikesEntity?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}