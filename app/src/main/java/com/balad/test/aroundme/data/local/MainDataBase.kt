package com.balad.test.aroundme.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.balad.test.aroundme.data.local.converter.*
import com.balad.test.aroundme.data.local.dao.RemoteDao
import com.balad.test.aroundme.data.local.dao.VenueDao
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.RemoteKey
import com.balad.test.aroundme.data.model.Venue
@Database(
        entities = [Venue::class, RemoteKey::class],
        version = 2,
        exportSchema = false
)
@TypeConverters(
        LocationConverter::class,
        StatsEntityConverter::class,
        ReasonsEntityConverter::class,
        SpecialsEntityConverter::class,
        LikesEntityConverter::class,
        HerenowEntityConverter::class,
        CategoryConverter::class,
        BestphotoEntityConverter::class,

)
abstract class MainDataBase: RoomDatabase() {
    abstract fun venueDao(): VenueDao
    abstract fun remoteDao(): RemoteDao
}