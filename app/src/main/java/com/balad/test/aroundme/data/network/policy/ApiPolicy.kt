package com.balad.test.aroundme.data.network.policy

import android.content.SharedPreferences
import android.location.Location
import com.balad.test.aroundme.utils.Constants
import com.balad.test.aroundme.utils.distance
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import javax.inject.Inject

class ApiPolicy @Inject constructor(private val sharedPreferences: SharedPreferences) {
    companion object{
        private const val LIMIT_CALL_DISTANCE = 100.0
    }
    fun accessCallRequest(location: com.balad.test.aroundme.data.model.Location): Boolean {
        val lastLocation = getLastLocation()
        var dictance = lastLocation?.distance(location.lat ?: 0.0, location.lng ?: 0.0)
        return dictance ?: LIMIT_CALL_DISTANCE + 1.0 > LIMIT_CALL_DISTANCE
    }

    private fun getLastLocation(): com.balad.test.aroundme.data.model.Location? {
        val lng = sharedPreferences.getString(Constants.PREF_LAST_LOCATION_LNG, null)
        val lat = sharedPreferences.getString(Constants.PREF_LAST_LOCATION_LAT, null)
        return com.balad.test.aroundme.data.model.Location(lat=lat?.toDouble(), lng = lng?.toDouble())
    }
}