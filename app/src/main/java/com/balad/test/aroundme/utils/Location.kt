package com.balad.test.aroundme.utils

import android.location.Location

fun com.balad.test.aroundme.data.model.Location.toComaFormat(): String{
    return run {
        this.lat.toString().plus(",").plus(this.lng)
    }
}

fun com.balad.test.aroundme.data.model.Location.distance(lat2: Double, lon2: Double): Double {
    val startPoint = Location("locationA")
    startPoint.latitude = this.lat ?: 0.0
    startPoint.longitude = this.lng ?: 0.0

    val endPoint = Location("locationB")
    endPoint.latitude = lat2 ?: 0.0
    endPoint.longitude = lon2 ?: 0.0
    return startPoint.distanceTo(endPoint).toDouble()
}

private fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}

private fun rad2deg(rad: Double): Double {
    return rad * 180.0 / Math.PI
}