package com.balad.test.aroundme.data.remote

interface IBuilder {
    fun build(): VenueRemoteMediator
}