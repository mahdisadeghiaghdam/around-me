package com.balad.test.aroundme.data.remote

interface IVenueRemoteBrowse: IBuilder, IVenueRemoteGlobal {
    fun setRadius(radius: Int): IVenueRemoteBrowse
}