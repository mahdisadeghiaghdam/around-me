package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.SpecialsEntity
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class SpecialsEntityConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: SpecialsEntity?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): SpecialsEntity ?{
            val listType = object : TypeToken<SpecialsEntity?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}