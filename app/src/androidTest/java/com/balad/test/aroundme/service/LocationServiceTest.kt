package com.balad.test.aroundme.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ServiceTestRule
import junit.framework.TestCase
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeoutException
import kotlin.jvm.Throws

@RunWith(AndroidJUnit4::class)
class LocationServiceTest : TestCase(){

    private lateinit var context: Context

    @get:Rule
    var mServiceRule = ServiceTestRule()

    @Before
    public override fun setUp() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    @Throws(TimeoutException::class)
    fun start_service_test() {
        LocationService.Builder(context).start()
    }

}