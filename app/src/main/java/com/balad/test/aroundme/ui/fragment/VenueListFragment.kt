package com.balad.test.aroundme.ui.fragment

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.balad.test.aroundme.R
import com.balad.test.aroundme.databinding.FragmentVenueListBinding
import com.balad.test.aroundme.service.BroadCastHandler
import com.balad.test.aroundme.service.LocationService
import com.balad.test.aroundme.ui.adapter.VenueAdapter
import com.balad.test.aroundme.ui.adapter.VenueLoadingState
import com.balad.test.aroundme.utils.SpaceItemDecoration
import com.balad.test.aroundme.viewmodel.VenueViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_venue_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException
import javax.inject.Inject


@AndroidEntryPoint
class VenueListFragment : Fragment() {

    private val PERMISSIO_REQUEST: Int = 100
    private var binding: FragmentVenueListBinding? = null

    @Inject
    lateinit var venueAdapter: VenueAdapter

    private val venueViewModel: VenueViewModel by viewModels()

    private val itemDecoration: RecyclerView.ItemDecoration by lazy { SpaceItemDecoration(32) }

    /**
     * Update location of device
     */
    private val mBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val location = BroadCastHandler.getLocation(intent)
            location?.let {
                venueViewModel.setLastLocation(location)
                getVenueList(location)
            }
        }
    }


    /**
     * Click listener for start search location before access permission
     */
    private val mSearchVenueClickListener = View.OnClickListener {
        startAnimation()
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
        ),
        PERMISSIO_REQUEST
        )
    }

    override fun onStart() {
        BroadCastHandler.registerUpdateLocation(
            requireContext().applicationContext,
            mBroadCastReceiver
        )
        super.onStart()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (checkPermissions()){
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                ),
                PERMISSIO_REQUEST
            )
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_venue_list, container, false)
        binding?.venueViewModel = venueViewModel

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initial()
        initialLastLocation()
        loadingAdapter()
    }

    private fun loadingAdapter() {
        lifecycleScope.launch {
            venueAdapter.loadStateFlow.collectLatest { loadState ->
                if (loadState.refresh is LoadState.Error){
                    val s = loadState.refresh as LoadState.Error
                    if (s.error is SocketTimeoutException){
                        venueViewModel.removeLastLocation()
                    }
                    var d = ""
                }
            }
        }
    }

    /**
     * initial last location
     */
    private fun initialLastLocation() {
        val lastLocation = venueViewModel.getLastLocation()
        lastLocation?.let {
            venueViewModel.getVenueList(lastLocation)
        }
    }

    /**
     * initial views
     */
    private fun initial() {
        CoroutineScope(Main).launch {
            binding?.apply {
                val permission = checkPermissions()
                gpFindLocation.isGone = permission

                fabFindLocation.setOnClickListener(mSearchVenueClickListener)
                fabFindLocation.setOnClickListener(mSearchVenueClickListener)

                venueAdapter.onItemClickListener = {
                    if (findNavController().currentDestination?.id == R.id.fragmentVenueList) {
                        findNavController().navigate(
                            VenueListFragmentDirections.actionFragmentVenueListToFragmentVenueDetail(
                                it.id
                            )
                        )
                    }
                }

                rvVenue.adapter = venueAdapter
                rvVenue.addItemDecoration(itemDecoration)
                venueAdapter.withMySpecificFooter(VenueLoadingState())
            }
        }
    }

    override fun onStop() {
        BroadCastHandler.unRegisterUpdateLocation(
            requireContext().applicationContext,
            mBroadCastReceiver
        )
        super.onStop()
    }

    /**
     * call browse api from server and update adapter list
     * @param location lastLocation of device
     */
    private fun getVenueList(location: Location) {
        lifecycleScope.launch {
            venueViewModel.getVenueList(com.balad.test.aroundme.data.model.Location(lat = location.latitude, lng = location.longitude)).collectLatest { pagingData ->
                endAnimation()
                venueAdapter.submitData(pagingData)
            }

        }
    }

    /**
     * Stop animation after get data from server
     */
    private fun endAnimation() {
        binding?.lottieAnimWave?.pauseAnimation()
        binding?.lottieAnim?.pauseAnimation()
        binding?.gpFindLocation?.isGone = true
        binding?.lottieAnimWave?.isGone = true
    }

    /**
     * Start animation after access permission
     * Start animation when open first time
     */
    private fun startAnimation() {
        binding?.lottieAnimWave?.playAnimation()
        binding?.lottieAnim?.playAnimation()
        binding?.gpFindLocation?.isVisible = true
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIO_REQUEST) {
            if (checkPermissions()) {
                LocationService.Builder(requireContext()).start()
            }
        }
    }

    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
    }
}