package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.StatsEntity
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class StatsEntityConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: StatsEntity?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): StatsEntity ?{
            val listType = object : TypeToken<StatsEntity?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}