package com.balad.test.aroundme.data.local.converter

import androidx.room.TypeConverter
import com.balad.test.aroundme.data.model.Category
import com.balad.test.aroundme.data.model.HerenowEntity
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class HerenowEntityConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun  fromInstant(value: HerenowEntity?): String {
            return GsonBuilder().create().toJson(value)
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(value: String): HerenowEntity ?{
            val listType = object : TypeToken<HerenowEntity?>(){}.type
            return GsonBuilder().create().fromJson(value, listType)
        }
    }
}