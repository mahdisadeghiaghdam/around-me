package com.balad.test.aroundme.data.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class Icon(
    @Expose
    @SerializedName("prefix")

    var prefix: String?,
    @Expose
    @SerializedName("suffix")

    var suffix: String?
)