package com.balad.test.aroundme.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SpecialsEntity(
        @Expose
        @SerializedName("count")
        var count: Int,
        @Expose
        @SerializedName("items")
        var items: List<String>)