package com.balad.test.aroundme.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.Looper
import com.google.android.gms.location.*
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LocationService : Service() {
    @Inject
    lateinit var locationRequest: LocationRequest

    @Inject
    lateinit var fusedLocationClient: FusedLocationProviderClient

    //location callback for receive and send updated location
    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            BroadCastHandler.sendUpdatedLocation(applicationContext, locationResult.lastLocation)
        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startLocationUpdate()
        return START_STICKY
    }

    /**
     * request update location with locationRequest rules and send result to mLocationCallback
     */
    @SuppressLint("MissingPermission")
    fun startLocationUpdate() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )
    }

    companion object {
        const val DEFAULT_INTERVAL_TIME = 1000L

        private fun init(context: Context) {
            startService(context)
        }

        private fun startService(context: Context) {
            context.startService(Intent(context, LocationService::class.java))
        }

        fun stopService(context: Context) {
            context.stopService(Intent(context, LocationService::class.java))
        }
    }

    /**
     * Start service builder
     * @param context context of application for startService
     */
    class Builder(var context: Context) : IBuilder {
        override fun start() {
            init(context)
        }

    }

    interface IBuilder {
        fun start()
    }

}